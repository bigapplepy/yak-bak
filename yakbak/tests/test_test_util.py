import pytest

from yakbak.settings import DbSettings, Settings
from yakbak.tests.util import change_setting, change_settings
from yakbak.types import Application


def test_change_settings_changes_settings(app: Application) -> None:
    original_url = app.settings.db.url

    with change_settings(app, {"db.url": "the new URL"}):
        assert app.settings.db.url == "the new URL"

    assert app.settings.db.url == original_url


def test_change_setting_can_change_a_whole_section(settings: Settings) -> None:
    db_settings = DbSettings(url="the new URL")
    new_settings = change_setting(settings, "db", db_settings)
    assert new_settings.db.url == "the new URL"


def test_change_setting_fails_with_invalid_section(settings: Settings) -> None:
    with pytest.raises(ValueError):
        change_setting(settings, "no_such_section.no_such_key", "any value")


def test_change_setting_fails_with_invalid_setting_key(settings: Settings) -> None:
    with pytest.raises(ValueError):
        change_setting(settings, "auth.no_such_key", "any value")
