from operator import attrgetter
from typing import List

from attr import attrib, attrs
from md_tw import TWMarkdown
from slugify import slugify
import flask

from yakbak.models import Talk, User


@attrs(frozen=True)
class SlugAndContent:
    slug: str = attrib()
    content: str = attrib()


def format_talk_for_jekyll(talk: Talk) -> SlugAndContent:
    """
    Return a slug and YAML content for a talk page on Jekyll.

    Speakers on the talk will be sorted according to their full name.
    """
    markdown_wrapper = TWMarkdown(tw_width=76)

    talk_template = flask.current_app.jinja_env.get_or_select_template("jekyll/talk.md")

    speakers = [ts.user for ts in talk.speakers]
    speakers.sort(key=attrgetter("fullname"))
    description = markdown_wrapper(talk.description)

    return SlugAndContent(
        slug=slugify(talk.title),
        content=talk_template.render(
            dict(talk=talk, speakers=speakers, description=description)
        ),
    )


def format_speaker_for_jekyll(speaker: User, talks: List[Talk]) -> SlugAndContent:
    """
    Return a slug and YAML content for a speaker page on Jekyll.

    Talks on the page will be sorted according to their title.

    """
    markdown_wrapper = TWMarkdown(tw_width=76)

    speaker_template = flask.current_app.jinja_env.get_or_select_template(
        "jekyll/speaker.md"
    )

    talks.sort(key=attrgetter("title"))
    speaker_bio = markdown_wrapper(speaker.speaker_bio or "")

    return SlugAndContent(
        slug=slugify(speaker.fullname),
        content=speaker_template.render(
            dict(speaker=speaker, talks=talks, speaker_bio=speaker_bio)
        ),
    )
