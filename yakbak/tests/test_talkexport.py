import frontmatter

from yakbak.models import Conference, InvitationStatus, Talk, TalkFormat, User
from yakbak.talkexport import format_speaker_for_jekyll, format_talk_for_jekyll
from yakbak.types import Application


def test_format_talk_for_jekyll(
    app: Application, conference: Conference, short_talk_format: TalkFormat
) -> None:
    alice = User(email="alice@example.com", fullname="Alice Example")
    bob = User(email="bob@example.com", fullname="Bob Example")

    talk = Talk(
        title="My Talk",
        talk_format=short_talk_format,
        conference=conference,
        description=(
            "This is a very long description that spans more than the "
            "76-character wrapping limit that we impose on Jekyll templates "
            "so that we can see the wrapping work in practice."
        ),
    )
    talk.add_speaker(bob, InvitationStatus.CONFIRMED)
    talk.add_speaker(alice, InvitationStatus.CONFIRMED)

    with app.app_context():
        page = format_talk_for_jekyll(talk)

    assert page.slug == "my-talk"

    parsed = frontmatter.loads(page.content)

    assert parsed["duration"] == 25
    assert parsed["speakers"] == ["Alice Example", "Bob Example"]
    assert parsed["title"] == talk.title
    assert parsed["type"] == "talk"
    assert parsed.content == (
        "This is a very long description that spans more than the 76-character\n"
        "wrapping limit that we impose on Jekyll templates so that we can see the\n"
        "wrapping work in practice."
    )


def test_format_speaker_for_jekyll(
    app: Application, conference: Conference, short_talk_format: TalkFormat
) -> None:
    alice = User(
        email="alice@example.com",
        fullname="Alice Example",
        speaker_bio=(
            "This is a very long speaker bio that spans more than the "
            "76-character wrapping limit that we impose on Jekyll templates "
            "so that we can see the wrapping work in practice."
        ),
    )

    talk1 = Talk(title="My Talk", talk_format=short_talk_format, conference=conference)
    talk1.add_speaker(alice, InvitationStatus.CONFIRMED)

    talk2 = Talk(
        title="My Other Talk", talk_format=short_talk_format, conference=conference
    )
    talk2.add_speaker(alice, InvitationStatus.CONFIRMED)

    with app.app_context():
        page = format_speaker_for_jekyll(alice, [talk2, talk1])

    assert page.slug == "alice-example"

    parsed = frontmatter.loads(page.content)

    assert parsed["name"] == "Alice Example"
    assert parsed["talks"] == ["My Other Talk", "My Talk"]
    assert parsed.content == (
        "This is a very long speaker bio that spans more than the 76-character\n"
        "wrapping limit that we impose on Jekyll templates so that we can see the\n"
        "wrapping work in practice."
    )
