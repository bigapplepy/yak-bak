from datetime import datetime, timedelta
from operator import attrgetter
from typing import Generator, Iterable
from unittest.mock import Mock, patch
import os.path

from _pytest.fixtures import FixtureRequest
from flask import abort, redirect
from flask_login import login_user
from werkzeug.test import Client
from werkzeug.wrappers import Response
import jinja2
import pytest

from yakbak import mail
from yakbak.auth import load_user
from yakbak.core import APP_CACHE, create_app
from yakbak.models import Conference, db, TalkFormat, User
from yakbak.settings import load_settings_file, Settings
from yakbak.types import Application


@pytest.fixture(scope="session")
def settings() -> Settings:
    here = os.path.dirname(__file__)
    test_toml = os.path.join(here, "yakbak.toml-test")
    return load_settings_file(test_toml)


@pytest.fixture
def app(request: FixtureRequest, settings: Settings) -> Iterable[Application]:
    APP_CACHE.clear()

    # do this ugliness since the attrs object is frozen
    if "POSTGRES_HOST" not in os.environ or "POSTGRES_5432_TCP_PORT" not in os.environ:
        raise RuntimeError(
            "Run tests with tox (pip install -r test-requirements.txt && tox)"
        )

    postgres_uri = (
        f"postgresql://yakbak:y4kb4k@{os.environ['POSTGRES_HOST']}:"
        f"{os.environ['POSTGRES_5432_TCP_PORT']}/yakbak_tox_test"
    )
    object.__setattr__(settings.db, "url", postgres_uri)

    flask_config = {"TESTING": True, "MAIL_SUPPRESS_SEND": True}
    app = create_app(settings, flask_config)

    db.engine.execute(
        "CREATE COLLATION case_insensitive "
        "(provider=icu, locale='und-u-ks-level2', deterministic=false)"
    )
    db.create_all()

    @request.addfinalizer
    def tear_down_database() -> None:
        db.drop_all()

    # views will expect that there's a conference object in the DB
    conference = Conference(
        cfp_email="cfp@example.com",
        conduct_email="conduct@example.com",
        full_name="PyGotham 2019",
        informal_name="PyGotham",
        recording_release_url="http://www.example.com/",
        twitter_username="@Example",
        website="http://www.example.com/",
        proposals_begin=datetime.utcnow() - timedelta(days=1),
        proposals_end=datetime.utcnow() + timedelta(days=1),
        talk_formats=[
            TalkFormat(length=25, format="Standard Talk"),
            TalkFormat(length=40, format="Long Talk"),
        ],
    )
    db.session.add(conference)
    db.session.commit()

    here = os.path.dirname(__file__)
    test_templates_dir = os.path.join(here, "templates")
    my_loader = jinja2.ChoiceLoader(
        [app.jinja_loader, jinja2.FileSystemLoader(test_templates_dir)]
    )
    # Bugbear doesn't like this, but mypy doesn't like the alternative.
    setattr(app, "jinja_loader", my_loader)  # NOQA: B010

    # cheeky: add a /test-login endpoint to the app,
    # logging in with social auth in tests is tough
    @app.route("/test-login/<user_id>")
    def test_login(user_id: str) -> Response:
        user = load_user(user_id)
        if not user:
            abort(401)
        login_user(user)
        return redirect("/")

    yield app

    # remove() clears any in-process state; because of where we
    # define the session, a new one is not created when we call
    # create_app()
    db.session.remove()
    db.drop_all()
    db.engine.execute("DROP COLLATION case_insensitive")


@pytest.fixture
def client(app: Application) -> Client:
    return app.test_client()


# TODO: This is a bit backward and should probably be the thing to
# create the conference rather than fetching it from the app.
@pytest.fixture
def conference(app: Application) -> Conference:
    """Return a test conference."""
    return Conference.query.order_by(Conference.created.desc()).first()


@pytest.fixture
def short_talk_format(conference: Conference) -> TalkFormat:
    return min(conference.talk_formats, key=attrgetter("length"))


@pytest.fixture
def long_talk_format(conference: Conference) -> TalkFormat:
    return max(conference.talk_formats, key=attrgetter("length"))


@pytest.fixture
def talk_format_id(short_talk_format: TalkFormat) -> int:
    return short_talk_format.talk_format_id


@pytest.yield_fixture
def send_mail() -> Generator[Mock, None, None]:
    with patch.object(mail, "send_mail") as send_mail:
        yield send_mail


@pytest.fixture
def user(app: Application) -> User:
    user = User(fullname="Test User", email="test@example.com")
    db.session.add(user)
    db.session.commit()

    return user


@pytest.fixture
def site_admin(app: Application) -> User:
    user = User(fullname="Admin User", email="admin@example.com", site_admin=True)
    db.session.add(user)
    db.session.commit()

    return user


@pytest.fixture
def authenticated_client(*, app: Application, user: User) -> Client:
    """Return a client authenticated with a user and without CSRF protection."""
    app.config["WTF_CSRF_ENABLED"] = False
    client = app.test_client()
    client.get(f"/test-login/{user.user_id}", follow_redirects=True)
    return client
