---
duration: {{ talk.talk_format.length }}
presentation_url:
room:
slot:
speakers:{% for speaker in speakers %}
- {{ speaker.fullname }}
{%- endfor %}
title: {{ talk.title|tojson }}
type: talk
video_url:
---
{{ description }}
