from collections import defaultdict
from datetime import datetime, timedelta
from typing import Dict, Iterable, List, Optional, TYPE_CHECKING
from urllib.parse import urlparse
import json
import os.path
import sys
import textwrap

import click

from yakbak.core import create_app
from yakbak.models import (
    Category,
    Conference,
    db,
    Talk,
    TalkFormat,
    UsedMagicLink,
    User,
    Vote,
)
from yakbak.review import write_anonymous_review_csv
from yakbak.settings import find_settings_file, load_settings_file
from yakbak.talkexport import format_speaker_for_jekyll, format_talk_for_jekyll

# TODO: remove once https://github.com/python/typeshed/pull/2958 is merged
if TYPE_CHECKING:

    class DateTime:
        pass

else:
    from click.types import DateTime


app = create_app(load_settings_file(find_settings_file()))


@app.cli.command()
def sync_db() -> None:
    here = os.path.dirname(__file__)
    root = os.path.join(here, "..")
    alembic_ini = os.path.abspath(os.path.join(root, "alembic.ini"))

    from alembic import command
    from alembic.config import Config

    alembic_cfg = Config(alembic_ini)
    command.upgrade(alembic_cfg, "head")

    # Social-Auth needs to have the User model already in existence,
    # so do this second to creating the yakbak-specific models
    from social_flask_sqlalchemy import models

    models.PSABase.metadata.create_all(db.engine)


@app.cli.command()
@click.argument("full_name")
@click.argument("informal_name")
@click.argument("website")
@click.argument("talk_formats")
@click.argument("recording_release_url")
@click.argument("cfp_email")
@click.argument("twitter_username")
@click.argument("conduct_email")
@click.option("--proposals-begin", type=DateTime(), help="interpreted as UTC")
@click.option("--proposals-end", type=DateTime(), help="interpreted as UTC")
@click.option("--voting-begin", type=DateTime(), help="interpreted as UTC")
@click.option("--voting-end", type=DateTime(), help="interpreted as UTC")
def add_conference(
    full_name: str,
    informal_name: str,
    website: str,
    talk_formats: str,
    recording_release_url: str,
    cfp_email: str,
    twitter_username: str,
    conduct_email: str,
    proposals_begin: Optional[datetime],
    proposals_end: Optional[datetime],
    voting_begin: Optional[datetime],
    voting_end: Optional[datetime],
) -> None:
    if twitter_username.startswith("@"):
        print("Twitter username should not start with '@'")
        sys.exit(1)

    try:
        formats = json.loads(talk_formats)
        assert isinstance(formats, dict)
        assert all(isinstance(k, str) for k in formats)
        assert all(isinstance(v, int) for v in formats.values())
    except Exception:
        print("talk_formats should be a JSON map of string description to int length")
        print('eg \'{"Short Talk": 10, "Standard Talk": 25}')
        sys.exit(1)

    conf = Conference(
        full_name=full_name,
        informal_name=informal_name,
        website=website,
        recording_release_url=recording_release_url,
        cfp_email=cfp_email,
        twitter_username=twitter_username,
        conduct_email=conduct_email,
        proposals_begin=proposals_begin,
        proposals_end=proposals_end,
        voting_begin=voting_begin,
        voting_end=voting_end,
    )
    for format, length in formats.items():
        conf.talk_formats.append(TalkFormat(format=format, length=length))

    db.session.add(conf)
    db.session.commit()


@app.cli.command()
@click.argument("older_than_days")
def clean_magic_links(older_than_days: str) -> None:
    now = datetime.utcnow()
    threshold = now - timedelta(days=int(older_than_days))
    UsedMagicLink.query.filter(UsedMagicLink.used_on <= threshold).delete()
    db.session.commit()


def _report_on_user(user_id: int) -> None:
    """
    Print a report on activity by the user to the terminal.

    """
    user = User.query.get(user_id)
    if user is None:
        print(f"No user with user_id {user_id}")
        sys.exit(1)

    has_profile = (
        bool(user.twitter_username)
        or bool(user.speaker_bio)
        or bool(user.demographic_survey)
    )
    has_talks = len(user.talks) > 0
    has_votes = user.votes.count() > 0
    has_reports = user.conduct_reports_made.count() > 0

    print("----------")
    print(f"User ID {user_id} {user}")
    if has_profile:
        bio = textwrap.fill(
            user.speaker_bio or "", initial_indent="    ", subsequent_indent="    "
        )
        demo_survey = "completed" if user.demographic_survey else "(none)"
        print(f"  Twitter username: {user.twitter_username}")
        print(f"  Demographic survey: {demo_survey}")
        print(f"  Speaker bio: \n\n{bio}\n")
    else:
        print("  No twitter username or speaker bio set")

    print()
    if has_talks:
        print("  Talks:")
        for talk_speaker in user.talks:
            talk = talk_speaker.talk
            num_speakers = len(talk.speakers)
            if num_speakers > 1:
                print(f"    {talk.title} with {num_speakers - 1} other speakers")
            else:
                print(f"    {talk.title}")
    else:
        print("  No talks.")

    print()
    if has_votes:
        print(f"  Votes: {user.votes.count()}")
    else:
        print("  No votes.")

    print()
    if has_reports:
        print(f"  Code of conduct reports: {user.conduct_reports_made.count()}")
    else:
        print("  No code of conduct reports.")

    print()


def _social_auth_disconnect(user_to_del: User) -> None:
    social_auth_entries = user_to_del.social_auth.all()
    if social_auth_entries:
        storage_cls = social_auth_entries[0].__class__
        for entry in social_auth_entries:
            backend_name = entry.provider
            if not storage_cls.allowed_to_disconnect(user_to_del, backend_name):
                raise Exception(
                    f"Social auth provider {entry.provider} prevented disconnection"
                )

        for entry in social_auth_entries:
            storage_cls.disconnect(entry)


def _merge_talks(user_to_del: User, user_to_keep: User) -> None:
    for talk_speaker in user_to_del.talks:
        talk = talk_speaker.talk
        if not talk.has_speaker(user_to_keep):
            talk.add_speaker(user_to_keep, talk_speaker.state)
        db.session.delete(talk_speaker)


def _merge_votes(user_to_del: User, user_to_keep: User) -> None:
    for vote in user_to_del.votes:
        if (
            Vote.query.filter(Vote.user == user_to_keep, Vote.talk == vote.talk).count()
            == 0
        ):
            vote.user_id = user_to_keep.user_id
            db.session.add(vote)
        else:
            db.session.delete(vote)


def _merge_conduct_reports(user_to_del: User, user_to_keep: User) -> None:
    for report in user_to_del.conduct_reports_made:
        report.user_id = user_to_keep.user_id
        db.session.add(report)


def _merge_profile(user_to_del: User, user_to_keep: User) -> None:
    if user_to_del.demographic_survey and not user_to_keep.demographic_survey:
        survey = user_to_del.demographic_survey
        survey.user_id = user_to_keep.user_id
        db.session.add(survey)
    elif user_to_del.demographic_survey:
        db.session.delete(user_to_del.demographic_survey)

    if user_to_del.twitter_username and not user_to_keep.twitter_username:
        user_to_keep.twitter_username = user_to_del.twitter_username

    if user_to_del.speaker_bio and not user_to_keep.speaker_bio:
        user_to_keep.speaker_bio = user_to_del.speaker_bio


@app.cli.command()
@click.argument("user_id_a", metavar="USER_ID", type=int)
@click.argument("user_id_b", metavar="USER_ID", type=int)
def merge_users(user_id_a: int, user_id_b: int) -> None:
    """
    Merge the two users.

    Prompt the person running this command to pick which user to keep,
    then reassign the items owned or associated with the other user to
    the first user.

    """
    _report_on_user(user_id_a)
    _report_on_user(user_id_b)

    print("----------")
    user_id_to_keep = int(input(f"Keep user_id {user_id_a} or user_id {user_id_b}? "))
    if user_id_to_keep == user_id_a:
        user_to_keep = User.query.get(user_id_a)
        user_to_del = User.query.get(user_id_b)
    elif user_id_to_keep == user_id_b:
        user_to_keep = User.query.get(user_id_b)
        user_to_del = User.query.get(user_id_a)
    else:
        print(f"user_id must be {user_id_a} or {user_id_b}")

    # to avoid order issues with foreign keys, delete or update all the
    # dependent objects in a nested transaction, commit that; then commit
    # the implicit outer transaction when deleting the user_to_del
    db.session.begin_nested()

    _social_auth_disconnect(user_to_del)
    _merge_talks(user_to_del, user_to_keep)
    _merge_votes(user_to_del, user_to_keep)
    _merge_conduct_reports(user_to_del, user_to_keep)
    _merge_profile(user_to_del, user_to_keep)

    # the nested transaction
    db.session.commit()

    db.session.refresh(user_to_del)
    db.session.delete(user_to_del)
    db.session.commit()


@app.cli.command()
@click.argument("base-url", type=str)
@click.option(
    "--category_id",
    type=int,
    help="If provided, export talks in a single category only",
)
def export_review_spreadsheet(base_url: str, category_id: Optional[int]) -> None:
    """
    Prepare CSV files for program committee review.

    Each file is named after a category, and contains a row for each talk in
    that category. Each row contains fields for talk ID, title, length, a link
    to review the proposal, and a summary of votes.

    The CSV is written to stdout.

    """

    if base_url is not None:
        parsed = urlparse(base_url)
        app.config["PREFERRED_URL_SCHEME"] = parsed.scheme
        app.config["SERVER_NAME"] = parsed.netloc

    category = Category.query.get(category_id)
    conference = Conference.query.order_by(Conference.created.desc()).first()
    with app.test_request_context():
        write_anonymous_review_csv(conference, category, sys.stdout)


@app.cli.command()
@click.argument("talk_dir", type=click.Path(exists=True, file_okay=False))
@click.argument("speaker_dir", type=click.Path(exists=True, file_okay=False))
@click.argument("talk_ids", nargs=-1, type=int, metavar="TALK_ID [TALK_ID ...]")
def export_talks(talk_dir: str, speaker_dir: str, talk_ids: Iterable[int]) -> None:
    """
    Export given talks to TALK_DIR, and speakers for those talks to SPEAKER_DIR.

    """

    speaker_to_talk: Dict[User, List[Talk]] = defaultdict(list)
    talks = Talk.query.filter(Talk.talk_id.in_(talk_ids))
    for talk in talks:
        page = format_talk_for_jekyll(talk)
        filename = f"{page.slug}.md"
        with open(os.path.join(talk_dir, filename), "w") as fp:
            fp.write(page.content)
        speakers = [ts.user for ts in talk.speakers]
        for speaker in speakers:
            speaker_to_talk[speaker].append(talk)

    for speaker, talks in speaker_to_talk.items():
        page = format_speaker_for_jekyll(speaker, talks)
        filename = f"{page.slug}.md"
        with open(os.path.join(speaker_dir, filename), "w") as fp:
            fp.write(page.content)
