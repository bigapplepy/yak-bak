"""normalize User.email

Revision ID: 6e1cb9f95ef0
Revises: aa460d47bb07
Create Date: 2020-03-29 15:31:59.359942

"""
import logging

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "6e1cb9f95ef0"
down_revision = "aa460d47bb07"
branch_labels = None
depends_on = None


log = logging.getLogger("alembic")


def fail_if_duplicate_emails_exist():
    conn = op.get_bind()
    result = list(
        conn.execute(
            """
        select a.user_id
             , a.email
             , b.user_id
             , b.email
          from public.user a
             , public.user b
         where lower(a.email) = lower(b.email)
           and a.user_id != b.user_id
         order by a.user_id asc
        """
        )
    )
    if result:
        log.error("Cannot migrate, users with conflicting email addresses found:")
        seen_duplicates = set()
        for a_user_id, a_email, b_user_id, b_email in result:
            if a_user_id in seen_duplicates:
                continue

            log.error(
                "User %d (%r) conflicts with user %d (%r)",
                a_user_id,
                a_email,
                b_user_id,
                b_email,
            )
            seen_duplicates.add(b_user_id)

        raise Exception(
            "One or more duplicate email addresses exist in user table, "
            "see above for details. You may use the `flask merge-users` "
            "script to review & update users with conflicting emails."
        )


def upgrade():
    fail_if_duplicate_emails_exist()

    op.execute(
        "CREATE COLLATION case_insensitive "
        "(provider=icu, locale='und-u-ks-level2', deterministic=false)"
    )
    op.alter_column(
        "user",
        "email",
        type_=sa.String(length=256, collation="case_insensitive"),
    )


def downgrade():
    op.alter_column(
        "user",
        "email",
        type_=sa.String(length=256),
    )
    op.execute("DROP COLLATION case_insensitive")
