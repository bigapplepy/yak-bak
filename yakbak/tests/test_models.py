from yakbak.models import Conference, db, Talk, User, Vote
from yakbak.types import Application


def test_talk_vote_score(
    app: Application, conference: Conference, talk_format_id: int
) -> None:
    talk = Talk(title="", talk_format_id=talk_format_id, conference=conference)

    users = [User(fullname="", email=f"{i}@example.com") for i in range(11)]

    db.session.add(talk)
    db.session.add_all(users)
    db.session.commit()  # establish talk_id and user_id

    vote_values = [1, 0, -1, None, 0, 0, 1, 0, 1, -1, 0]
    votes = [
        Vote(
            talk_id=talk.talk_id,
            user_id=u.user_id,
            value=v if v is not None else None,
            skipped=v is None,
        )
        for u, v in zip(users, vote_values)
    ]

    db.session.add_all(votes)
    db.session.commit()

    talk = Talk.query.first()

    # the count and denominators are 10, skipped votes aren't counted
    assert talk.vote_count == 10
    assert talk.vote_score == 0.1  # 1 / 10
    assert talk.upvote_score == 0.3  # 3 / 10
    assert talk.downvote_score == 0.2  # 2 / 10
    assert talk.indifferent_score == 0.5  # 5 / 10


def test_talk_vote_score_with_no_votes(
    app: Application, conference: Conference, talk_format_id: int
) -> None:
    # there should be no division by zero errors
    talk = Talk(title="", talk_format_id=talk_format_id, conference=conference)
    db.session.add(talk)
    db.session.commit()

    assert talk.vote_count == 0
    assert talk.vote_score == 0
    assert talk.upvote_score == 0
    assert talk.downvote_score == 0
    assert talk.indifferent_score == 0
