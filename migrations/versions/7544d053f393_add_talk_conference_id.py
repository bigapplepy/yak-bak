"""add Talk.conference_id

Revision ID: 7544d053f393
Revises: aa460d47bb07
Create Date: 2020-05-30 11:09:31.838224

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "7544d053f393"
down_revision = "6e1cb9f95ef0"
branch_labels = None
depends_on = None


def upgrade():
    # at this time, there should be 1 and only 1 conference in the db, as
    # yak-bak previously did not support multiple conferences in any way
    conn = op.get_bind()
    result = conn.execute("SELECT COUNT(*) FROM conference")
    (num_confs,) = list(result)[0]
    if num_confs == 0:
        # this is the case in `tox -e migrate` for instance
        result = conn.execute("SELECT COUNT(*) FROM talk")
        (num_talks,) = list(result)[0]
        assert (
            num_talks == 0
        ), "If there are no Conferences, there should be no Talks in the database"
    else:
        assert num_confs == 1, "There should be exactly 1 Conference in the database"

    op.add_column("talk", sa.Column("conference_id", sa.Integer(), nullable=True))
    op.execute(
        "UPDATE talk SET conference_id=(SELECT conference_id FROM conference LIMIT 1)"
    )
    op.alter_column("talk", "conference_id", nullable=False)
    op.create_foreign_key(
        "talk_conference_id_fkey",
        "talk",
        "conference",
        ["conference_id"],
        ["conference_id"],
    )


def downgrade():
    op.drop_constraint("talk_conference_id_fkey", "talk", type_="foreignkey")
    op.drop_column("talk", "conference_id")
