---
name: {{ speaker.fullname }}
talks:{% for talk in talks %}
- {{ talk.title|tojson }}
{%- endfor %}
---
{{ speaker_bio }}
