from contextlib import contextmanager
from typing import Any, Dict, Generator, Pattern, Union
import re

from attr import evolve
from flask import Response

from yakbak.settings import Settings
from yakbak.types import Application


@contextmanager
def change_settings(
    app: Application, changed_settings: Dict[str, Any]
) -> Generator[None, None, None]:
    """
    Temporarily change settings on the application.

    """
    original_settings = app.settings

    settings = original_settings
    for key, value in changed_settings.items():
        settings = change_setting(settings, key, value)

    app.settings = settings
    try:
        yield
    finally:
        app.settings = original_settings


def change_setting(root_settings: Settings, setting: str, replacement: Any) -> Settings:
    # the settings tree is at most 2 parts deep; the section & key
    section_path, _, key_path = setting.partition(".")

    try:
        section = getattr(root_settings, section_path)
        section_type = type(section)
    except AttributeError:
        raise ValueError(f"Settings has no section {section_path}")

    if key_path:
        if not hasattr(section, key_path):
            raise ValueError(f"{section_type} has no key {key_path}")
        section = evolve(section, **{key_path: replacement})

    else:
        if type(replacement) != type(section):
            raise ValueError(f"{replacement!r} must be of type {section_type}")
        section = replacement

    return evolve(root_settings, **{section_path: section})


def assert_html_response(resp: Response, status: int = 200) -> str:
    """
    Ensure ``resp`` has certain common HTTP headers for HTML responses.

    Returns the decoded HTTP response body.

    """
    assert resp.status_code == status, f"got {resp.status_code}"
    assert resp.headers["Content-Type"].startswith("text/html")
    if status == 200:
        assert "charset" in resp.headers["Content-Type"]

    data = resp.data
    assert resp.content_length == len(data)

    return data.decode(resp.charset or "utf8")


def assert_html_response_contains(
    resp: Response, *snippets: Union[str, Pattern], status: int = 200
) -> None:
    """
    Ensure that each of the ``snippets`` is in the body of ``resp``.

    """
    content = assert_html_response(resp, status)
    for snippet in snippets:
        if isinstance(snippet, Pattern):
            assert re.search(
                snippet, content
            ), f"{snippet!r} does not match {content!r}"
        else:
            assert snippet in content


def assert_html_response_doesnt_contain(
    resp: Response, *snippets: Union[str, Pattern], status: int = 200
) -> None:
    """
    Ensure that none of the ``snippets`` is in the body of ``resp``.

    """
    content = assert_html_response(resp, status)
    for snippet in snippets:
        if isinstance(snippet, Pattern):
            assert not re.search(
                snippet, content
            ), f"{snippet!r} should not match {content!r}"
        else:
            assert snippet not in content


def assert_redirected(resp: Response, to: str) -> None:
    assert_html_response(resp, 302)

    landing_url = f"http://localhost{to}"
    assert resp.headers["Location"] == landing_url


def extract_csrf_from(resp: Response) -> str:
    data = resp.data
    body = data.decode(resp.mimetype_params["charset"])
    tags = re.findall('(<input[^>]*name="csrf_token"[^>]*>)', body)
    csrfs = set()
    for tag in tags:
        assert 'type="hidden"' in tag
        match = re.search('value="([^"]*)"', tag)
        assert match, "CSRF hidden input had no value"
        csrfs.add(match.group(1))

    assert len(csrfs) == 1, f"Expected 1 CSRF tag value, got {csrfs}"
    return csrfs.pop()
