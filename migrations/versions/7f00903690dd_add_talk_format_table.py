"""add_talk_format_table

Revision ID: 7f00903690dd
Revises: 7544d053f393
Create Date: 2023-02-15 08:43:42.227691

"""
from itertools import groupby
from operator import itemgetter
import json

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = "7f00903690dd"
down_revision = "7544d053f393"
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()

    op.create_table(
        "talk_format",
        sa.Column("talk_format_id", sa.Integer(), nullable=False),
        sa.Column("conference_id", sa.Integer(), nullable=False),
        sa.Column("format", sa.String(length=256), nullable=False),
        sa.Column("length", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["conference_id"],
            ["conference.conference_id"],
        ),
        sa.PrimaryKeyConstraint("talk_format_id"),
        sa.UniqueConstraint(
            "talk_format_id", "conference_id", name="uix_talk_format_id_conference_id"
        ),
    )

    # Conference.talk_lengths -> talk_formats
    rows = conn.execute("SELECT conference_id, talk_lengths FROM conference")
    for conference_id, lengths in rows.fetchall():
        for length in lengths:
            format = f"{length} minutes"
            sql = (
                "INSERT INTO talk_format (conference_id, format, length) "
                "VALUES (%(conference_id)s, %(format)s, %(length)s)"
            )
            conn.execute(sql, conference_id=conference_id, format=format, length=length)

    op.drop_column("conference", "talk_lengths")

    # set Talk.talk_format_id
    op.add_column("talk", sa.Column("talk_format_id", sa.Integer(), nullable=True))
    rows = conn.execute(
        "SELECT t.talk_id, f.talk_format_id "
        "FROM talk t "
        "JOIN talk_format f ON "
        "(f.conference_id = t.conference_id AND f.length = t.length)"
    )
    for talk_id, talk_format_id in rows.fetchall():
        sql = (
            "UPDATE talk SET talk_format_id = %(talk_format_id)s "
            "WHERE talk_id = %(talk_id)s"
        )
        conn.execute(sql, talk_format_id=talk_format_id, talk_id=talk_id)

    op.alter_column("talk", "talk_format_id", nullable=False)

    op.create_foreign_key(
        "talk_talk_format_id_fkey",
        "talk",
        "talk_format",
        ["talk_format_id"],
        ["talk_format_id"],
    )
    op.create_foreign_key(
        "talk_conference_id_talk_format_id_fkey",
        "talk",
        "talk_format",
        ["conference_id", "talk_format_id"],
        ["conference_id", "talk_format_id"],
    )
    op.drop_column("talk", "length")


def downgrade():
    conn = op.get_bind()

    # restore Talk.length
    op.add_column(
        "talk", sa.Column("length", sa.INTEGER(), autoincrement=False, nullable=True)
    )
    rows = conn.execute(
        "SELECT t.talk_id, f.length FROM talk t "
        "JOIN talk_format f ON (f.talk_format_id = t.talk_format_id)"
    )
    for talk_id, length in rows.fetchall():
        conn.execute(
            "UPDATE talk SET length = %(length)s WHERE talk_id = %(talk_id)s",
            length=length,
            talk_id=talk_id,
        )

    op.alter_column("talk", "length", nullable=False)

    op.drop_constraint(
        "talk_conference_id_talk_format_id_fkey", "talk", type_="foreignkey"
    )
    op.drop_constraint("talk_talk_format_id_fkey", "talk", type_="foreignkey")
    op.drop_column("talk", "talk_format_id")

    # restore Conference.talk_lengths
    op.add_column(
        "conference",
        sa.Column("talk_lengths", sa.JSON(), autoincrement=False, nullable=True),
    )
    rows = conn.execute(
        "SELECT conference_id, length FROM talk_format ORDER BY conference_id, length"
    )
    groups = groupby(rows, key=itemgetter(0))
    for conference_id, subrows in groups:
        lengths = [r[1] for r in subrows]
        if len(lengths) != len(set(lengths)):
            raise Exception(
                f"Cannot migrate down, conference {conference_id} "
                f"has duplicate talk lengths: {lengths}"
            )
        conn.execute(
            "UPDATE conference SET talk_lengths = %(talk_lengths)s "
            "WHERE conference_id = %(conference_id)s",
            talk_lengths=json.dumps(lengths),
            conference_id=conference_id,
        )

    op.alter_column("conference", "talk_lengths", nullable=False)

    op.drop_table("talk_format")
