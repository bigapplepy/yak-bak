from yakbak import mail
from yakbak.types import Application


def test_send_mail(app: Application) -> None:
    with app.app_context(), app.test_request_context():
        with mail.mail.record_messages() as outbox:
            mail.send_mail(
                to=["test@example.com"],
                template="email-template",  # in yakbak/tests/templates
                variables="replacements",
            )

    assert len(outbox) == 1

    msg = outbox[0]
    assert msg.subject == "The Email Subject"
    assert msg.sender == "sender@example.com"
    assert msg.recipients == ["test@example.com"]
    assert msg.body == "This is the email. It has replacements!"


def test_message_body_textwrapping(app: Application) -> None:
    body = (
        "I am the very model of a modern major general "
        "I've information vegetable animal and mineral "
        "I know the kings of England and I quote the fights historical "
        "From Marathon to Waterloo in order categorical "
    )
    with app.app_context(), app.test_request_context():
        with mail.mail.record_messages() as outbox:
            mail.send_mail(
                to=["test@example.com"],
                template="email-template",  # in yakbak/tests/templates
                variables=body,
            )

    assert len(outbox) == 1
    msg = outbox[0]

    assert msg.body == (
        "This is the email. It has "
        "I am the very model of a modern major general\n"
        "I've information vegetable animal and mineral "
        "I know the kings of\n"
        "England and I quote the fights historical "
        "From Marathon to Waterloo in\n"
        "order categorical !"
    )


def test_wrap_body() -> None:
    body = (
        "I am the very model of a modern major general "
        "I've information vegetable animal and mineral "
        "I know the kings of England and I quote the fights historical "
        "From Marathon to Waterloo in order categorical"
        "\n\n"
        "I'm very well acquainted, too, with matters mathematical "
        "I understand equations, both the simple and quadratical "
        "About binomial theorem I am teeming with a lot o' news "
        "With many cheerful facts about the square of the hypotenuse"
        "\n\n\n"
        "Thanks!"
    )

    expected = (
        "I am the very model of a modern major general I've information vegetable\n"
        "animal and mineral I know the kings of England and I quote the fights\n"
        "historical From Marathon to Waterloo in order categorical"
        "\n\n"
        "I'm very well acquainted, too, with matters mathematical I understand\n"
        "equations, both the simple and quadratical About binomial theorem I am\n"
        "teeming with a lot o' news With many cheerful facts about the square of\n"
        "the hypotenuse"
        "\n\n\n"
        "Thanks!"
    )

    wrapped = mail.wrap_body(body)
    assert wrapped == expected


def test_wrap_body_doesnt_wrap_very_long_urls() -> None:
    # our magic link URLs can be quite long, and can contain
    # some non-word characters
    body = (
        "Log in at https://www.example.com/"
        "IAmTheVeryModelOfA/ModernMajorGeneral."
        "IveInformationVegetableAnimalAndMineral-"
        "IKnowTheKingsOf?EnglandAndIQuote=TheFightsHistorical"
    )

    expected = (
        "Log in at\n"
        "https://www.example.com/"
        "IAmTheVeryModelOfA/ModernMajorGeneral."
        "IveInformationVegetableAnimalAndMineral-"
        "IKnowTheKingsOf?EnglandAndIQuote=TheFightsHistorical"
    )

    wrapped = mail.wrap_body(body)
    assert wrapped == expected
