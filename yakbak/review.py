from io import StringIO
from typing import Optional, TextIO
import csv

from flask import url_for

from yakbak.models import Conference, Talk, TalkCategory


def get_anonymous_review_csv(
    conference: Conference, category: Optional[TalkCategory] = None
) -> str:
    """
    Return contents of a CSV for anonymous talk review, optionally filtered by category.

    See :func:`write_anonymous_review_csv` for details.
    """

    out = StringIO()
    write_anonymous_review_csv(conference, category, out)
    return out.getvalue()


def write_anonymous_review_csv(
    conference: Conference, category: Optional[TalkCategory], fp: TextIO
) -> None:
    """
    Write contents of a CSV for anonymous talk review to the ``fp``.

    The CSV has columns:
    * Talk ID
    * Speakers
    * Title
    * Categories
    * Format
    * Length
    * Link
    * Upvote Share
    * Neutral Share
    * Downvote Share
    * Net Score
    """

    writer = csv.writer(fp)
    writer.writerow(
        [
            "Talk ID",
            "Speakers",
            "Title",
            "Categories",
            "Format",
            "Length",
            "Link",
            "Upvote Share",
            "Neutral Share",
            "Downvote Share",
            "Net Score",
        ]
    )

    talks = Talk.query.active_for(conference)
    if category is not None:
        talks = talks.filter(Talk.categories.contains(category))

    for talk in talks.order_by(Talk.talk_id):
        categories = [c.name for c in talk.categories]
        categories.sort()
        speakers = [ts.user.user_id for ts in talk.speakers]
        speakers.sort()
        writer.writerow(
            [
                str(talk.talk_id),
                ", ".join(map(str, speakers)),
                talk.title,
                "; ".join(categories),
                talk.talk_format.format,
                str(talk.talk_format.length),
                url_for(
                    "views.review_talk",
                    talk_id=talk.talk_id,
                    _external=True,
                ),
                f"{talk.upvote_score*100:.2f}",
                f"{talk.indifferent_score*100:.2f}",
                f"{talk.downvote_score*100:.2f}",
                f"{talk.vote_score*100:.2f}",
            ]
        )
