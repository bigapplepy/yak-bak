from datetime import datetime, timedelta

from werkzeug.test import Client
import pytest

from yakbak import auth
from yakbak.models import Conference, db, InvitationStatus, Talk, User
from yakbak.tests.util import (
    assert_html_response,
    change_settings,
    extract_csrf_from,
)
from yakbak.types import Application


@pytest.mark.parametrize(
    "length, expected",
    [
        (60, "1 minute"),
        (120, "2 minutes"),
        (3600, "1 hour"),
        (7200, "2 hours"),
        (86400, "1 day"),
        (172800, "2 days"),
        (31536000, "1 year"),
        (63072000, "2 years"),
        # test that it doesn't do "2 days, 4 hours, 10 minutes..."
        (187800, "2 days"),
    ],
)
def test_get_magic_link_token_expiry(
    app: Application, length: int, expected: str
) -> None:
    with app.app_context(), change_settings(
        app, {"auth.email_magic_link_expiry": length, "auth.signing_key": "abcd"}
    ):
        _, expiry = auth.get_magic_link_token_and_expiry("test@example.com")

    assert expiry == expected


def test_parse_magic_link_token(app: Application) -> None:
    with app.app_context(), change_settings(
        app, {"auth.email_magic_link_expiry": 10, "auth.signing_key": "abcd"}
    ):
        token, _ = auth.get_magic_link_token_and_expiry("test@example.com")
        email = auth.parse_magic_link_token(token)

        assert email == "test@example.com"


def test_parse_magic_link_token_is_none_for_garbled_tokens(app: Application) -> None:
    with app.app_context(), change_settings(
        app, {"auth.email_magic_link_expiry": 10, "auth.signing_key": "abcd"}
    ):
        token, _ = auth.get_magic_link_token_and_expiry("test@example.com")
        token = token[:-2]
        email = auth.parse_magic_link_token(token)

        assert email is None


def test_parse_magic_link_token_is_none_for_expired_tokens(app: Application) -> None:
    with app.app_context(), change_settings(
        app, {"auth.email_magic_link_expiry": -1, "auth.signing_key": "abcd"}
    ):
        token, _ = auth.get_magic_link_token_and_expiry("test@example.com")
        email = auth.parse_magic_link_token(token)

        assert email is None


def test_talk_creation_only_allowed_in_window(user: User, client: Client) -> None:
    conf = Conference.query.get(1)
    conf.proposals_begin = datetime.utcnow() - timedelta(days=1)
    conf.proposals_end = datetime.utcnow() + timedelta(days=1)
    db.session.add(conf)
    db.session.commit()

    client.get("/test-login/{}".format(user.user_id), follow_redirects=True)
    resp = client.get("/talks/new")
    assert_html_response(resp, status=200)


def test_talk_creation_not_allowed_out_of_window(user: User, client: Client) -> None:
    conf = Conference.query.get(1)
    conf.proposals_begin = datetime.utcnow() - timedelta(days=3)
    conf.proposals_end = datetime.utcnow() - timedelta(days=1)
    db.session.add(conf)
    db.session.commit()

    client.get("/test-login/{}".format(user.user_id), follow_redirects=True)
    resp = client.get("/talks/new")
    assert_html_response(resp, status=400)


def test_talk_content_editing_not_allowed_while_voting(
    user: User, client: Client, talk_format_id: int
) -> None:
    conf = Conference.query.get(1)
    conf.voting_begin = datetime.utcnow() - timedelta(days=1)
    conf.voting_end = datetime.utcnow() + timedelta(days=1)
    db.session.add(conf)

    talk = Talk(
        title="My Talk",
        talk_format_id=talk_format_id,
        conference=conf,
        accepted_recording_release=True,
    )
    talk.add_speaker(user, InvitationStatus.CONFIRMED)
    db.session.add(talk)
    db.session.commit()

    db.session.refresh(talk)
    client.get("/test-login/{}".format(user.user_id), follow_redirects=True)
    resp = client.get(f"/talks/{talk.talk_id}")
    assert_html_response(resp, status=200)

    postdata = {
        "csrf_token": extract_csrf_from(resp),
        "title": "Changed title!",
        "accepted_recording_release": "checked",
    }
    resp = client.post(f"/talks/{talk.talk_id}", data=postdata)
    assert_html_response(resp, status=400)


def test_talk_content_editing_allowed_after_proposal_window_but_before_voting_window(
    user: User, client: Client, talk_format_id: int
) -> None:
    conf = Conference.query.get(1)
    conf.proposals_begin = datetime.utcnow() - timedelta(days=3)
    conf.proposals_end = datetime.utcnow() - timedelta(days=1)
    conf.voting_begin = datetime.utcnow() + timedelta(days=1)
    conf.voting_end = datetime.utcnow() + timedelta(days=3)
    db.session.add(conf)

    talk = Talk(
        title="My Talk",
        talk_format_id=talk_format_id,
        conference=conf,
        accepted_recording_release=True,
    )
    talk.add_speaker(user, InvitationStatus.CONFIRMED)
    db.session.add(talk)
    db.session.commit()

    db.session.refresh(talk)
    client.get("/test-login/{}".format(user.user_id), follow_redirects=True)
    resp = client.get(f"/talks/{talk.talk_id}")
    assert_html_response(resp, status=200)

    postdata = {
        "csrf_token": extract_csrf_from(resp),
        "title": "Changed title!",
        "accepted_recording_release": "checked",
    }
    resp = client.post(f"/talks/{talk.talk_id}", data=postdata, follow_redirects=True)
    assert_html_response(resp, status=200)


@pytest.mark.xfail(reason="Recording release is always required for PyGotham TV")
def test_talk_recording_release_editing_allowed_after_proposal_edit_window(
    user: User, client: Client, talk_format_id: int
) -> None:
    conf = Conference.query.get(1)
    conf.proposals_begin = datetime.utcnow() - timedelta(days=3)
    conf.proposals_end = datetime.utcnow() - timedelta(days=1)
    conf.voting_begin = datetime.utcnow() + timedelta(days=1)
    conf.voting_end = datetime.utcnow() + timedelta(days=3)
    db.session.add(conf)

    talk = Talk(
        title="My Talk",
        talk_format_id=talk_format_id,
        conference=conf,
        accepted_recording_release=False,
    )
    talk.add_speaker(user, InvitationStatus.CONFIRMED)
    db.session.add(talk)
    db.session.commit()

    db.session.refresh(talk)
    client.get("/test-login/{}".format(user.user_id), follow_redirects=True)
    resp = client.get(f"/talks/{talk.talk_id}")
    assert_html_response(resp, status=200)

    postdata = {
        "csrf_token": extract_csrf_from(resp),
        "accepted_recording_release": "checked",
    }
    resp = client.post(f"/talks/{talk.talk_id}", data=postdata, follow_redirects=True)
    assert_html_response(resp, status=200)


def test_talk_extra_requirements_editing_allowed_after_proposal_edit_window(
    user: User, client: Client, talk_format_id: int
) -> None:
    conf = Conference.query.get(1)
    conf.proposals_begin = datetime.utcnow() - timedelta(days=3)
    conf.proposals_end = datetime.utcnow() - timedelta(days=1)
    conf.voting_begin = datetime.utcnow() + timedelta(days=1)
    conf.voting_end = datetime.utcnow() + timedelta(days=3)
    db.session.add(conf)

    talk = Talk(
        title="My Talk",
        talk_format_id=talk_format_id,
        conference=conf,
        accepted_recording_release=True,
    )
    talk.add_speaker(user, InvitationStatus.CONFIRMED)
    db.session.add(talk)
    db.session.commit()

    db.session.refresh(talk)
    client.get("/test-login/{}".format(user.user_id), follow_redirects=True)
    resp = client.get(f"/talks/{talk.talk_id}")
    assert_html_response(resp, status=200)

    postdata = {
        "csrf_token": extract_csrf_from(resp),
        "requirements": "New requirements",
        "accepted_recording_release": "checked",
    }
    resp = client.post(f"/talks/{talk.talk_id}", data=postdata, follow_redirects=True)
    assert_html_response(resp, status=200)


def test_email_normalization_finds_correct_user(
    app: Application, user: User, client: Client
) -> None:
    with app.app_context(), change_settings(
        app, {"auth.email_magic_link_expiry": 30, "auth.signing_key": "abcd"}
    ):
        token, _ = auth.get_magic_link_token_and_expiry(user.email.upper())

        resp = client.get(f"/login/token/{token}", follow_redirects=True)
        assert_html_response(resp, status=200)
