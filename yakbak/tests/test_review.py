from datetime import datetime, timedelta
from typing import List, Optional
import re

from werkzeug.test import Client
import pytest

from yakbak.models import (
    Category,
    Conference,
    db,
    Talk,
    TalkStatus,
    User,
    Vote,
)
from yakbak.review import get_anonymous_review_csv
from yakbak.tests.util import (
    assert_html_response_contains,
    assert_html_response_doesnt_contain,
)
from yakbak.types import Application


def add_commit_refresh(instances: List[db.Model]) -> None:
    db.session.add_all(instances)
    db.session.commit()
    for instance in instances:
        db.session.refresh(instance)


@pytest.fixture
def categories(app: Application) -> List[Category]:
    conference = Conference.query.one()
    categories = [
        Category(name="Category 1", conference=conference),
        Category(name="Category 2", conference=conference),
        Category(name="Category 3", conference=conference),
    ]
    add_commit_refresh(categories)
    return categories


@pytest.fixture
def voters(app: Application) -> List[User]:
    voters = [
        User(fullname="Voter 1", email="voter1@example.com"),
        User(fullname="Voter 2", email="voter2@example.com"),
        User(fullname="Voter 3", email="voter3@example.com"),
        User(fullname="Voter 4", email="voter4@example.com"),
    ]
    add_commit_refresh(voters)
    return voters


@pytest.fixture
def talks(
    app: Application, categories: List[Category], talk_format_id: int
) -> List[Talk]:
    conference = Conference.query.one()
    talks = [
        Talk(
            title="Talk 1",
            talk_format_id=talk_format_id,
            categories=[categories[0]],
            conference=conference,
        ),
        Talk(
            title="Talk 2",
            talk_format_id=talk_format_id,
            categories=[categories[0]],
            conference=conference,
        ),
        Talk(
            title="Talk 3",
            talk_format_id=talk_format_id,
            categories=[categories[1]],
            conference=conference,
        ),
        Talk(
            title="Talk 4",
            talk_format_id=talk_format_id,
            categories=[categories[1]],
            conference=conference,
        ),
        Talk(
            title="Talk 5",
            talk_format_id=talk_format_id,
            categories=[categories[1]],
            conference=conference,
            state=TalkStatus.WITHDRAWN,
        ),
        # leave one category empty of talks
    ]
    add_commit_refresh(talks)
    return talks


@pytest.fixture
def votes(app: Application, talks: List[Talk], voters: List[User]) -> List[Vote]:
    votes = [
        # talk 1: vote score +1
        Vote(talk_id=talks[0].talk_id, user_id=voters[0].user_id, value=0),
        Vote(talk_id=talks[0].talk_id, user_id=voters[1].user_id, skipped=True),
        Vote(talk_id=talks[0].talk_id, user_id=voters[2].user_id, value=1),
        Vote(talk_id=talks[0].talk_id, user_id=voters[3].user_id, value=0),
        # talk 2: vote score -2
        Vote(talk_id=talks[1].talk_id, user_id=voters[0].user_id, value=-1),
        Vote(talk_id=talks[1].talk_id, user_id=voters[1].user_id, value=-1),
        Vote(talk_id=talks[1].talk_id, user_id=voters[2].user_id, value=0),
        # talk 3: vote score 0
        Vote(talk_id=talks[2].talk_id, user_id=voters[1].user_id, value=1),
        Vote(talk_id=talks[2].talk_id, user_id=voters[2].user_id, skipped=True),
        Vote(talk_id=talks[2].talk_id, user_id=voters[3].user_id, value=-1),
        # talk 4: no votes
        # talk 5: no votes, withdrawn
    ]
    add_commit_refresh(votes)
    return votes


@pytest.fixture(autouse=True)
def enable_talk_review_and_voting(app: Application) -> None:
    """Enable talk review for the test conference."""
    conference = Conference.query.first()
    conference.review_begin = datetime.utcnow() - timedelta(days=1)
    conference.review_end = datetime.utcnow() + timedelta(days=2)
    conference.voting_begin = datetime.utcnow() - timedelta(days=1)
    conference.voting_end = datetime.utcnow() + timedelta(days=2)
    db.session.commit()


@pytest.mark.parametrize(
    "vote_value,skipped,vote_note",
    (
        (1, False, "You voted: Definitely yes!"),
        (0, False, "You voted: I&#39;m impartial."),
        (-1, False, "You voted: Definitely not."),
        (None, True, "You skipped voting on this talk."),
        (None, False, "You did not vote on this talk."),
    ),
)
def test_vote_review_shows_your_vote_and_conduct_form(
    *,
    vote_value: Optional[int],
    skipped: bool,
    vote_note: str,
    authenticated_client: Client,
    user: User,
    conference: Conference,
    talk_format_id: int,
) -> None:
    """Show the user's vote results, if present, and the conduct report form."""
    talk = Talk(
        title="",
        talk_format_id=talk_format_id,
        is_anonymized=True,
        anonymized_title="",
        anonymized_description="",
        anonymized_outline="",
        anonymized_take_aways="",
        conference=conference,
    )
    conference = Conference.query.first()
    category = Category(name="", conference=conference)
    category.talks.append(talk)
    db.session.add_all((category, talk))
    if vote_value is not None:
        vote = Vote(talk=talk, user=user, value=vote_value, skipped=False)
        db.session.add(vote)
    elif skipped:
        vote = Vote(talk=talk, user=user, value=None, skipped=True)
        db.session.add(vote)
    db.session.commit()

    resp = authenticated_client.get(f"/review/talk/{talk.talk_id}")
    assert_html_response_contains(resp, vote_note)
    assert_html_response_contains(resp, '<form method="POST" action="/conduct-report">')


def test_review_home(authenticated_client: Client, categories: List[Category]) -> None:
    assert len(categories) == 3

    resp = authenticated_client.get("/review")
    assert_html_response_contains(
        resp,
        re.compile(
            f'<a href="/review/category/{categories[0].category_id}">\\s*'
            f"{categories[0].name}\\s*</a>"
        ),
        re.compile(
            f'<a href="/review/category/{categories[1].category_id}">\\s*'
            f"{categories[1].name}\\s*</a>"
        ),
        re.compile(
            f'<a href="/review/category/{categories[2].category_id}">\\s*'
            f"{categories[2].name}\\s*</a>"
        ),
    )


def test_review_category_list(
    client: Client, talks: List[Talk], voters: List[User], votes: List[Vote]
) -> None:
    category = talks[0].categories[0]
    talks_in_category = [t for t in talks if category in t.categories]
    assert len(talks_in_category) == 2

    talk1, talk2 = talks_in_category

    client.get(f"/test-login/{voters[1].user_id}")
    resp = client.get(f"/review/category/{category.category_id}")
    assert_html_response_contains(
        resp,
        re.compile(f'<a href="/review/talk/{talk1.talk_id}">\\s*{talk1.title}\\s*</a>'),
        re.compile(f'<a href="/review/talk/{talk2.talk_id}">\\s*{talk2.title}\\s*</a>'),
    )


def test_uncategorized_talks(
    site_admin: User, client: Client, talks: List[Talk], categories: List[Category]
) -> None:
    assert all(t.categories != [] for t in talks)

    client.get(f"/test-login/{site_admin.user_id}")

    resp = client.get("/review")
    assert_html_response_doesnt_contain(
        resp,
        re.compile(
            '<a href="/review/uncategorized">.*Uncategorized Talks.*</a>', re.DOTALL
        ),
        re.compile("Some talks are uncategorized, you should"),
        re.compile('<a href="/manage/categorize">categorize them</a>'),
    )

    # make one talk uncategorized
    del talks[0].categories[:]
    db.session.add(talks[0])
    db.session.commit()

    db.session.refresh(talks[0])

    resp = client.get("/review")
    assert_html_response_contains(
        resp,
        re.compile(
            '<a href="/review/uncategorized">.*Uncategorized Talks.*</a>', re.DOTALL
        ),
        re.compile("Some talks are uncategorized, you should"),
        re.compile('<a href="/manage/categorize">categorize them</a>'),
    )


def test_review_csv_for_everything(
    app: Application, conference: Conference, talks: List[Talk]
) -> None:
    with app.test_request_context():
        review_csv = get_anonymous_review_csv(conference)

    lines = review_csv.splitlines()
    _, body = lines[0], lines[1:]

    talk_ids = set([int(line.split(",", 1)[0]) for line in body])
    assert talk_ids == set(
        [t.talk_id for t in talks if t.state != TalkStatus.WITHDRAWN]
    )


def test_review_csv_for_single_category(
    app: Application,
    conference: Conference,
    talks: List[Talk],
    categories: List[Category],
) -> None:
    category = categories[0]
    with app.test_request_context():
        review_csv = get_anonymous_review_csv(conference, category)

    lines = review_csv.splitlines()
    _, body = lines[0], lines[1:]

    talk_ids = set([int(line.split(",", 1)[0]) for line in body])

    expected_talks = set([talks[0].talk_id, talks[1].talk_id])
    assert talk_ids == expected_talks
