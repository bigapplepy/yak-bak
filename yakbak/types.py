from typing import Tuple, Union

from flask import Flask
from werkzeug.wrappers import Response as FlaskResponse

from yakbak.settings import Settings

# A subset of the valid return types from a Flask view function; see
# https://github.com/python/typeshed/blob/master/third_party/2and3/flask/app.pyi
# for all the gnarly glory. We shadow the name "Response" rather than something
# more specific to make code using this type clearer
Response = Union[FlaskResponse, str, Tuple[str, int]]


class Application(Flask):

    """
    Custom :class:`~flask.Flask` sub-class with some convenience attributes.

    """

    def __init__(self, settings: Settings) -> None:
        super(Application, self).__init__("yakbak")
        self.settings = settings
