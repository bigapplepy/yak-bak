from datetime import datetime, timedelta
from unittest.mock import ANY, Mock

from werkzeug.test import Client

from yakbak.models import Conference, db, InvitationStatus, Talk, User
from yakbak.tests.util import (
    assert_html_response,
    assert_html_response_contains,
    assert_redirected,
    extract_csrf_from,
)


def test_anonymous_users_cant_access_admin(client: Client) -> None:
    resp = client.get("/manage/")
    assert_html_response(resp, status=404)


def test_ordinary_users_cant_access_admin(client: Client, user: User) -> None:
    client.get("/test-login/{}".format(user.user_id), follow_redirects=True)
    resp = client.get("/manage/")
    assert_html_response(resp, status=404)


def test_site_admins_can_access_admin(client: Client, user: User) -> None:
    client.get("/test-login/{}".format(user.user_id), follow_redirects=True)

    user.site_admin = True
    db.session.add(user)
    db.session.commit()

    resp = client.get("/manage/")
    assert_html_response(resp, status=200)


def test_talk_anonymization(
    client: Client,
    user: User,
    send_mail: Mock,
    conference: Conference,
    talk_format_id: int,
) -> None:
    user.site_admin = True
    db.session.add(user)

    talk = Talk(
        title="Alice's Identifying Talk",
        description="This talk is by Alice",
        outline="Alice!",
        take_aways="Alice's point.",
        talk_format_id=talk_format_id,
        conference=conference,
    )
    talk.add_speaker(user, InvitationStatus.CONFIRMED)
    db.session.add(talk)
    db.session.commit()

    db.session.refresh(talk)

    client.get("/test-login/{}".format(user.user_id), follow_redirects=True)
    resp = client.get(f"/manage/anonymize/{talk.talk_id}")
    assert_html_response_contains(resp, "Alice&#39;s Identifying Talk")

    postdata = {
        "title": "(Speaker name redacted)'s Identifying Talk",
        "description": "This talk is by (Speaker name redacted)",
        "outline": "(Speaker name redacted)!",
        "take_aways": "(The speaker's) point.",
        "csrf_token": extract_csrf_from(resp),
    }
    client.post(f"/manage/anonymize/{talk.talk_id}", data=postdata)

    talk = Talk.query.get(talk.talk_id)
    assert talk.is_anonymized is True
    assert talk.has_anonymization_changes is True
    assert talk.anonymized_title == "(Speaker name redacted)'s Identifying Talk"
    assert talk.anonymized_description == "This talk is by (Speaker name redacted)"
    assert talk.anonymized_outline == "(Speaker name redacted)!"
    assert talk.anonymized_take_aways == "(The speaker's) point."
    assert talk.title == "Alice's Identifying Talk"
    assert talk.description == "This talk is by Alice"
    assert talk.outline == "Alice!"
    assert talk.take_aways == "Alice's point."

    send_mail.assert_called_once_with(
        to=[user.email],
        template="email/talk-anonymized",
        talk_id=talk.talk_id,
        title=talk.title,  # the original title
    )


def test_talk_anonymization_doesnt_set_is_anonymized_if_no_changes(
    client: Client,
    user: User,
    send_mail: Mock,
    conference: Conference,
    talk_format_id: int,
) -> None:
    user.site_admin = True
    db.session.add(user)

    talk = Talk(
        title="Alice's Identifying Talk",
        description="This talk is by Alice",
        outline="Alice!",
        take_aways="Alice's point.",
        talk_format_id=talk_format_id,
        conference=conference,
    )
    talk.add_speaker(user, InvitationStatus.CONFIRMED)
    db.session.add(talk)
    db.session.commit()

    db.session.refresh(talk)

    client.get("/test-login/{}".format(user.user_id), follow_redirects=True)
    resp = client.get(f"/manage/anonymize/{talk.talk_id}")
    assert_html_response_contains(resp, "Alice&#39;s Identifying Talk")

    postdata = {
        "title": talk.title,
        "description": talk.description,
        "outline": talk.outline,
        "take_aways": talk.take_aways,
        "csrf_token": extract_csrf_from(resp),
    }
    client.post(f"/manage/anonymize/{talk.talk_id}", data=postdata)

    talk = Talk.query.get(talk.talk_id)
    assert talk.is_anonymized is True
    assert talk.has_anonymization_changes is False
    assert talk.anonymized_title == talk.title
    assert talk.anonymized_description == talk.anonymized_description
    assert talk.anonymized_outline == talk.outline
    assert talk.anonymized_take_aways == talk.take_aways

    assert not send_mail.called


def test_talk_feedback_not_available_when_voting_enabled(
    client: Client,
    user: User,
    site_admin: User,
    conference: Conference,
    talk_format_id: int,
    send_mail: Mock,
) -> None:
    conference.voting_begin = datetime.utcnow() - timedelta(days=1)
    conference.voting_end = datetime.utcnow() + timedelta(days=1)
    db.session.add(conference)

    talk = Talk(
        title="Alice's Identifying Talk",
        description="This talk is by Alice",
        outline="Alice!",
        take_aways="Alice's point.",
        talk_format_id=talk_format_id,
        conference=conference,
        anonymized_title="Alice's Identifying Talk",
        anonymized_description="This talk is by Alice",
        anonymized_outline="Alice!",
        anonymized_take_aways="Alice's point.",
        is_anonymized=True,
    )
    talk.add_speaker(user, InvitationStatus.CONFIRMED)
    db.session.add(talk)
    db.session.commit()
    db.session.refresh(talk)

    # before we submit any feedback, show a placeholder note
    client.get("/test-login/{}".format(site_admin.user_id), follow_redirects=True)

    resp = client.get(f"/review/talk/{talk.talk_id}")
    assert_html_response_contains(resp, "No feedback sent for this talk")
    assert_html_response_contains(
        resp, "New feedback cannot be sent to speakers during the voting window"
    )


def test_talk_feedback_sends_email(
    client: Client,
    user: User,
    site_admin: User,
    conference: Conference,
    talk_format_id: int,
    send_mail: Mock,
) -> None:
    user_email = user.email

    talk = Talk(
        title="Alice's Identifying Talk",
        description="This talk is by Alice",
        outline="Alice!",
        take_aways="Alice's point.",
        talk_format_id=talk_format_id,
        conference=conference,
        anonymized_title="Alice's Identifying Talk",
        anonymized_description="This talk is by Alice",
        anonymized_outline="Alice!",
        anonymized_take_aways="Alice's point.",
        is_anonymized=True,
    )
    talk.add_speaker(user, InvitationStatus.CONFIRMED)
    db.session.add(talk)
    db.session.commit()
    db.session.refresh(talk)

    # before we submit any feedback, show a placeholder note
    client.get("/test-login/{}".format(site_admin.user_id), follow_redirects=True)

    resp = client.get(f"/review/talk/{talk.talk_id}")
    assert_html_response_contains(resp, "No feedback sent for this talk")
    assert_html_response_contains(
        resp, f'<form method="POST" action="/manage/send_feedback/{talk.talk_id}"'
    )

    postdata = {
        "subject": "Feedback on your talk",
        "message": "Feedback message body",
        "csrf_token": extract_csrf_from(resp),
    }
    resp = client.post(f"/manage/send_feedback/{talk.talk_id}", data=postdata)
    assert_redirected(resp, "/manage/")

    send_mail.assert_called_once_with(
        to=[user_email],
        template="email/contact-speakers",
        sender=conference.cfp_email,
        subject="Feedback on your talk",
        message="Feedback message body",
        talk=ANY,
    )

    # due to session quirkiness, the talk object saved within the mock's call
    # history is not refreshable nor its talk_id attribute accessible :(
