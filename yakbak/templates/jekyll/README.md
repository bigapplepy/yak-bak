# Templates for Jekyll-based sites

The `export-talks` command can produce talk and speaker descriptions
in a format used by the PyGotham Jekyll site. This directory contains
templates for producing those files.
